Tips for using these files:

- Include Settings.cs in your GXP project (it will take care of the settings loading by itself). 

- Include the settings.txt file (or your own variant of it) in bin/Debug, or in bin/Release. (Or in a subfolder of these - in that case, add the folder name to the file name in Settings.cs: e.g. SettingsFileName="Assets/settings.txt")

- See MyGame.cs and Player.cs for usage examples. Some common values are static fields (Settings.Width and Settings.Height, etc), other values need to be retrieved using methods (e.g. Settings.GetInt("ScreenResolutionX") and Settings.GetBool("FullScreen")).

- If you initialize your game as shown in MyGame.cs (which is recommended), then *don't use game.width and game.height in your game logic code*! Use Settings.Width and Settings.Height instead.

- The given settings file shows the resolution of the arcade machine (1600 x 1200, so aspect ratio 4:3), and the standard mapping of joysticks/buttons to keys that will be used.

- If you want to change settings.txt: follow the syntax of the given file! (For example: comments start with "//", and go on separate lines.) Settings.cs will figure out on its own whether a value is an int / float / bool or string.

- Feel free to use this settings file for other dynamic settings. For example, include a line: "level1=Assets/map3.tmx" - then you can choose the levels that should be loaded without recompiling the code or renaming level files.

- Retrieving settings using the Settings.cs methods (e.g. Settings.GetBool("FullScreen")) is not superefficient. Don't use this for parameters that are retrieved (multiple times) every frame! (Bad example: Settings.GetFloat("ParticleSpeed").) Copy such settings to a field of the appropriate class instead.