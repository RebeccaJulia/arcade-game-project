﻿using GXPEngine;
using System.Drawing;

class Player : Canvas {
	const int radius = 30;

	float speed = 15;

	readonly int up;
	readonly int left;
	readonly int right;
	readonly int down;
	readonly int fire1;
	readonly int fire2;
	readonly int fire3;
	readonly int fire4;
	readonly int fire5;
	readonly int fire6;
	readonly int reset;
	readonly int startX, startY;

	public Player(int playerNumber, int pStartX, int pStartY) : base(2*radius,2*radius) {
		SetOrigin(radius, radius);

		graphics.FillEllipse (
			new SolidBrush(Color.White),
			0, 0, 2 * radius, 2 * radius
		);

		x = pStartX;
		y = pStartY;
		startX = pStartX;
		startY = pStartY;

		if (playerNumber == 1) {
			up = Settings.P1Up;
			left = Settings.P1Left;
			down = Settings.P1Down;
			right = Settings.P1Right;
			fire1 = Settings.P1Fire1;
			fire2 = Settings.P1Fire2;
			fire3 = Settings.P1Fire3;
			fire4 = Settings.P1Fire4;
			fire5 = Settings.P1Fire5;
			fire6 = Settings.P1Fire6;
			reset = Settings.Start1P;
		} else {
			up = Settings.P2Up;
			left = Settings.P2Left;
			down = Settings.P2Down;
			right = Settings.P2Right;
			fire1 = Settings.P2Fire1;
			fire2 = Settings.P2Fire2;
			fire3 = Settings.P2Fire3;
			fire4 = Settings.P2Fire4;
			fire5 = Settings.P2Fire5;
			fire6 = Settings.P2Fire6;
			reset = Settings.Start2P;
		}
		System.Console.WriteLine("Creating player {0} at {1},{2}",playerNumber,pStartX,pStartY);
	}

	public void Update() {
		if (Input.GetKey(up)) { 
			y -= speed; 
			if (y - radius < 0) y = radius; 
		}
		if (Input.GetKey(down)) { 
			y += speed; 
			if (y + radius >= Settings.Height) y = Settings.Height-radius;
		}
		if (Input.GetKey(left)) { 
			x -= speed; 
			if (x - radius < 0) x = radius; 
		}
		if (Input.GetKey(right)) { 
			x += speed; 
			if (x + radius >= Settings.Width) x = Settings.Width-radius;
		}
		if (Input.GetKeyDown(fire1)) color = 0xff0000ff;
		if (Input.GetKeyDown(fire2)) color = 0xff00ff00;
		if (Input.GetKeyDown(fire3)) color = 0xffff0000;
		if (Input.GetKeyDown(fire4)) color = 0xff00ffff;
		if (Input.GetKeyDown(fire5)) color = 0xffff00ff;
		if (Input.GetKeyDown(fire6)) color = 0xffffff00;
		if (Input.GetKeyDown(reset) || Input.GetKeyDown(Settings.Menu)) {
			x = startX;
			y = startY;
			color = 0xffffffff;
		}
	}
}