﻿using System;
using GXPEngine;

class MyGame : Game {
	public MyGame() : base(Settings.GetInt("ScreenResolutionX"),
	                       Settings.GetInt("ScreenResolutionY"),
	                       Settings.GetBool("FullScreen")) {
		AddChild(new Player(1,   Settings.Width/3, Settings.Height/2));
		AddChild(new Player(2, 2*Settings.Width/3, Settings.Height/2));
	}

	public void Update() {
		for (int i = 0; i < 400; i++) {
			if (Input.GetKeyDown(i))
				Console.WriteLine("Key pressed: number " + i);
		}	}

	public static void Main() {
		Game myGame=new MyGame();
		myGame.scaleX = 1f*Settings.GetInt("ScreenResolutionX") / Settings.Width;
		myGame.scaleY = 1f*Settings.GetInt("ScreenResolutionY") / Settings.Height;
		myGame.Start();
	}
}